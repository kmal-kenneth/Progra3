﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUnoDos
{
    class Menu
    {
        public void Mostrar()
        {
            Console.WriteLine("\nMENU \n");

            int i = 1;
            Console.WriteLine(" {0} Saludo.", i++);
            Console.WriteLine(" {0} Leer 3 números.", i++);
            Console.WriteLine(" {0} Leer un numero.", i++);
            Console.WriteLine(" {0} Invertir los digitos de un numero.", i++);
            Console.WriteLine(" {0} Calculos con una lista de números.", i++);
            Console.WriteLine(" {0} Maximo comun divisor.", i++);
            Console.WriteLine(" {0} Cuantas parejas habran despues de N meses.", i++);
            Console.WriteLine(" {0} Cuantas mese se requieren para tener N parejas.", i++);
            Console.WriteLine(" {0} Es primo.", i++);
            Console.WriteLine(" {0} Tiempo en encontrar los 10000 primeros números primos.", i++);
            Console.WriteLine(" {0} Es perfecto.", i++);
            Console.WriteLine(" {0} Salir del menú.", i);
            Console.WriteLine(" Cualquier otro valor limpia la consola.");
            Console.Write("\n Seleccione una opción:");

            string s = Console.ReadLine();

            Console.WriteLine("");

            switch (s)
            {
                case "1":
                    Menusaludo();
                    break;
                case "2":
                    MenuTresNumeros();
                    break;
                case "3":
                    Console.WriteLine("El número es: {0}",LeerInt("Ingrese un numero:"));
                    break;
                case "4":
                    MenuInvertir();
                    break;
                case "5":
                    MenuLista();
                    break;
                case "6":
                    MenuMaximoComunDivisor();
                    break;
                case "7":
                    MenuParejasNMesese();
                    break;
                case "8":
                    MenuMesesnParejas();
                    break;
                case "9":
                    MenuPrimo();
                    break;
                case "10":
                    MenuDiezMilPrimos();
                    break;
                case "11":
                    MenuPerfecto();
                    break;
                case "12":
                    return;
                default:
                    Console.Clear();
                    break;
            }

            Mostrar();
        }

        private void MenuPerfecto()
        {
            int x = LeerInt("Número a evaluar: ");
            List<int> list = new List<int>();

            for(int i = x -1; i > 0; i--)
            {
                if(x % i == 0)
                {
                    list.Add(i);
                }
            }

            int suma = 0;

            foreach(int i in list)
            {
                suma += i;
            }

            if ( suma == x)
            {
                Console.WriteLine("El {0} es perfecto.", x);
            }
            else
            {
                Console.WriteLine("El {0} no es perfecto.", x);
            }

        }

        private void MenuDiezMilPrimos()
        {
            TimeSpan stop;
            TimeSpan start = new TimeSpan(DateTime.Now.Ticks);

            int primos = 0;
            int i = 0;
            while(primos < 10000)
            {
                bool primo = Primo(i);
                if (primo)
                {
                    primos++;
                }
                i++;
            }

            stop = new TimeSpan(DateTime.Now.Ticks);
            Console.WriteLine("Duracion en milisegundos: {0}",stop.Subtract(start).TotalMilliseconds);
        }

        private void MenuPrimo()
        {
            int x = LeerInt("Número a evaluar: ");

            if(x < 0) { return; }

            bool primo = Primo(x);

            if (primo)
            {
                Console.WriteLine("El {0} es primo.", x);
            }
            else
            {
                Console.WriteLine("El {0} no es primo.", x);
            }
        }

        private bool Primo (int num)
        {
            bool primo = true;
            for (int i = num - 1; primo && i > 1; i--)
            {
                primo = (num % i == 0) ? false : true;
            }

            return primo;
        }

        private void MenuMesesnParejas()
        {
            Console.WriteLine("Suponiendo que cada mes nace una pareja y no mueren.");
            int x = LeerInt("Número de parejas: ");

            int parejas = 0;
            int meses = 0;
            while (parejas < x)
            {
                meses++;
                parejas = Fibonacci(meses);
            }

            Console.WriteLine("El número de meses es: {0}", meses);
        }

        private void MenuParejasNMesese()
        {
            Console.WriteLine("Suponiendo que cada mes nace una pareja y no mueren.");
            int x = LeerInt("Número de meses: ");

            int parejas = Fibonacci(x);

            Console.WriteLine("El número de parejas es: {0}", parejas);
        }

        private int Fibonacci(int numero)
        {
            if (numero == 0)
            {
                return 0;
            }
            else if (numero == 1)
            {
                return 1;
            }
            else
            {
                return Fibonacci(numero - 1) + Fibonacci(numero - 2);
            } 
        }

        private void MenuMaximoComunDivisor()
        {
            int x = LeerInt("Primer número: ");
            int y = LeerInt("Segundo número: ");

            int maximo = 0;

            if (x > y)
            {
                maximo = Divisor(y, x);
            }

            else
            {
                maximo = Divisor(x, y);
            }

            Console.WriteLine("El maximo comun divisor de {0} y {1} es: {2}", x, y, maximo);
        }

        private int Divisor(int menor, int mayor)
        {
            int i = menor;
            int divisor = 0;
            while (i > 0)
            {
                if (menor % i == 0 && mayor % i == 0)
                {
                    divisor = i;
                    break;
                }

                i--;
            }

            return divisor;
        }

        private void MenuLista()
        {
            int x = LeerInt("Candidad de números: ");

            if (x <= 0) { return; }

            int[] lista = new int[x];

            for (int i = 0;  i < x; i++)
            {
                string s =  i+1 + "º número de la lista: ";
                lista[i] = LeerInt(s);
            }

            Array.Sort(lista);

            float media = 0;

            foreach(int i in lista)
            {
                media += i;
            }

            media /= x;

            Console.WriteLine("El menor es {0}, el mayor es {1} y la media es {2}", lista[0], lista[x-1], media);
        }

        private void MenuInvertir()
        {
            string x = LeerInt("Número a invertir: ").ToString();
            string y = "";

            for (int i = x.Length; i > 0; i--)
            {
                y += x[i-1];
            }

            Console.WriteLine("El número invertido es: {0}", y);
        }

        private int LeerInt(string frase)
        {
            Console.WriteLine(frase);
            string s = Console.ReadLine();
            int i = 0;

            try
            {
                i = Convert.ToInt32(s);
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e);
            }
            return i;
        }

        private void MenuTresNumeros()
        {
            Console.Write("Primer número : ");
            int i =  Convert.ToInt32(Console.ReadLine());
            Console.Write("\nSegundo número : ");
            int j = Convert.ToInt32(Console.ReadLine());
            Console.Write("\nTercero número : ");
            int k = Convert.ToInt32(Console.ReadLine());

            int resultado = 0;

            if(i > 0)
            {
                resultado = j * k;
                Console.WriteLine("El producto es: {0}", resultado);
            }
            else
            {
                resultado = j + k;
                Console.WriteLine("La suma es: {0}", resultado);
            }

            Console.Write("\n Desea repetir la acción (s/n): ");
            string o = Console.ReadLine();

            if (o == "s" || o == "S")
            {
                MenuTresNumeros();
            }
        }

        private void Menusaludo()
        {
            Console.Write("Nombre de Usuario: ");
            string s = Console.ReadLine();
            DateTime fecha = DateTime.Now;

            Console.WriteLine("\nBienvenido {0}. {1:MM/dd/yy H:mm:ss zzz}\n ", s, fecha);
        }
    }
}
