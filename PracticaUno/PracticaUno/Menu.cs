﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Menu
    {
        private Operaciones operaciones;

        public Menu()
        {
            this.operaciones = new Operaciones();
        }

        public void Mostrar()
        {
            Console.WriteLine("MENU \n");

            int i = 1;
            Console.WriteLine(" {0} Hacer una suma donde el primer numero digitado se sumara con el segundo numero digitado.", i++);
            Console.WriteLine(" {0} Hacer una resta donde el primer numero digitado se restara con el segundo numero digitado.", i++);
            Console.WriteLine(" {0} Hacer una multiplicación donde el primer numero digitado se multiplicara con el segundo numero digitado.", i++);
            Console.WriteLine(" {0} Hacer una división donde el primer numero digitado se dividirá con el segundo numero digitado.", i++);
            Console.WriteLine(" {0} Llenar una arreglo con un numero n donde n es un numero digitado por el usuario.", i++);
            Console.WriteLine(" {0} Imprimir el arreglo.", i++);
            Console.WriteLine(" {0} Salir del menú.", i);
            Console.WriteLine(" Cualquier otro valor limpia la consola.");
            Console.Write("\n Seleccione una opción:");

            string s = Console.ReadLine();

            switch (s)
            {
                case "1":
                    MenuSuma();
                    break;
                case "2":
                    MenuResta();
                    break;
                case "3":
                    MenuMultiplicacion();
                    break;
                case "4":
                    MenuDivicion();
                    break;
                case "5":
                    MenuArreglo();
                    break;
                case "6":
                    MenuImprimirArreglo();
                    break;
                case "7":
                    return;
                default:
                    Console.Clear();
                    break;
            }

            Mostrar();
        }

        private void MenuSuma()
        {
            Console.Write("\n   Digite el primer numero:");
            float num1 = Convert.ToSingle(Console.ReadLine());
            Console.Write("   Digite el segundo numero:");
            float num2 = Convert.ToSingle(Console.ReadLine());

            float resultado = operaciones.SumaDosNumeros(num1, num2);

            Console.WriteLine("   La suma de {0} y {1} es: {2}\n", num1, num2, resultado);
        }

        private void MenuResta()
        {
            Console.Write("\n   Digite el primer numero:");
            float num1 = Convert.ToSingle(Console.ReadLine());
            Console.Write("   Digite el segundo numero:");
            float num2 = Convert.ToSingle(Console.ReadLine());

            float resultado = operaciones.RestaDosNumeros(num1, num2);

            Console.WriteLine("   La resta de {0} y {1} es: {2}\n", num1, num2, resultado);
        }

        private void MenuMultiplicacion()
        {
            Console.Write("\n   Digite el primer numero:");
            float num1 = Convert.ToSingle(Console.ReadLine());
            Console.Write("   Digite el segundo numero:");
            float num2 = Convert.ToSingle(Console.ReadLine());

            float resultado = operaciones.MultiplicaDosNumeros(num1, num2);

            Console.WriteLine("   La multiplicación de {0} por {1} es: {2}\n", num1, num2, resultado);
        }

        private void MenuDivicion()
        {
            Console.Write("\n   Digite el primer numero:");
            float num1 = Convert.ToSingle(Console.ReadLine());
            Console.Write("   Digite el segundo numero:");
            float num2 = Convert.ToSingle(Console.ReadLine());

            float resultado = operaciones.DivideDosNumeros(num1, num2);

            Console.WriteLine("   La divición de {0} entre {1} es: {2}\n", num1, num2, resultado);
        }

        private void MenuArreglo()
        {
            Console.Write("\n   Digite el numero:");
            float num = Convert.ToSingle(Console.ReadLine());          

            operaciones.CrearArreglo(num);

            Console.WriteLine("");
        }

        private void MenuImprimirArreglo()
        {
            float[] resultado = operaciones.Arreglo;

            if (resultado != null)
            {
                string s = "[";

                foreach (float i in resultado)
                {
                    if (i == resultado[resultado.Length - 1])
                    {
                        s += " " + i;
                    }
                    else
                    {
                        s += " " + i + ",";
                    }
                }

                s += "]";

                Console.WriteLine("   El arreglo es: {0}\n", s);
            }
            else
            {
                Console.WriteLine("   El arreglo esta vacio.\n");
            }
        }
    }
}
