﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Operaciones
    {
        private float[] arreglo;

        public float[] Arreglo
        {
            get
            {
                return arreglo;
            }

            set
            {
                arreglo = value;
            }
        }

        public float SumaDosNumeros(float num1, float num2)
        {
            return num1 + num2;
        }

        public float RestaDosNumeros(float num1, float num2)
        {
            return num1 - num2;
        }

        public float MultiplicaDosNumeros(float num1, float num2)
        {
            return num1 * num2;
        }

        public float DivideDosNumeros(float num1, float num2)
        {
            return num1 / num2;
        }

        public void CrearArreglo(float numero)
        {
            int x = Convert.ToInt32(numero);
            Console.WriteLine(x);
            arreglo = new float[x];

            for (int i = 0; i < x; i++)
            {
                arreglo[i] = numero;
            }

        }
    }
}
