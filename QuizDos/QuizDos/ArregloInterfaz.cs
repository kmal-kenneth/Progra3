﻿using QuizDos.Utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizDos
{
    public partial class ArregloInterfaz : Form
    {
        ArregloUtilidades utilidades;
        int[] arreglo;

        public ArregloInterfaz()
        {
            InitializeComponent();
            utilidades = new ArregloUtilidades();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (numericUpDownElementos.Value <= 0)
            {
                return;
            }

            arreglo = utilidades.GenerarArreglo((int)numericUpDownElementos.Value);

            richTextBox1.Text = utilidades.ImprimirArreglo(arreglo);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(arreglo == null)
            {
                return;
            }

            richTextBox1.Text =  utilidades.ImprimirArreglo(utilidades.Ordenar(arreglo));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (arreglo == null)
            {
                return;
            }

            richTextBox2.Text = utilidades.Rango(arreglo);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value <= 0)
            {
                return;
            }

            arreglo = utilidades.GenerarArreglo((int)numericUpDownElementos.Value);

            richTextBox2.Text = utilidades.ImprimirArreglo(arreglo);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            arreglo = utilidades.GenerarArreglo(10);
            richTextBox3.Text = utilidades.Promedio(arreglo);
        }
    }
}
