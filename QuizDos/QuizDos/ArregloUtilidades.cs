﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizDos.Utilidades
{
    class ArregloUtilidades
    {
        internal int[] GenerarArreglo(int numElementos)
        {
            int[] arreglo = new int[numElementos];

            Random rnd = new Random();
            for (int i = 0; i < numElementos; i++)
            {
                arreglo[i] = rnd.Next(1000);
            }

            return arreglo;
        }

        internal string ImprimirArreglo(int[] arreglo)
        {
            string texto = "";
            if (arreglo != null)
            {
                foreach (var item in arreglo)
                {
                    texto += item + ",";
                }
            }
            return texto.Length > 0 ? texto.Substring(0, texto.Length - 1) : texto;
        }


        internal int[] Ordenar(int[] arreglo)
        {
            Array.Sort(arreglo);
            return arreglo;
        }

        internal string Rango(int[] arreglo)
        {
            Ordenar(arreglo);
            return ImprimirArreglo(arreglo) + "\nRango: " + (arreglo[arreglo.Length - 1 ] - arreglo[0]).ToString();
        }

        internal string Promedio(int[] arreglo)
        {
            float promedio = 0;

            foreach(int i in arreglo)
            {
                promedio += i;
            }

            promedio /= arreglo.Length;

            return ImprimirArreglo(arreglo) + String.Format("\nPromedio: {0:0.00}", promedio);
        }
    }
}
