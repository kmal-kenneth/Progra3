﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuizDos.Utilidades;

namespace QuizDos
{
    public partial class NumeroInterfaz : Form
    {
        private NumeroUtilidades utilidades;

        public NumeroInterfaz()
        {
            InitializeComponent();
            utilidades = new Utilidades.NumeroUtilidades();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (utilidades.EsNarcisista(textBox1.Text)){
                label1.Text = "Narcisista";
            }
            else
            {
                label1.Text = "No Narcisista";
            }
        }

        private void RaizAproximada(object sender, EventArgs e)
        {
            if(textBox2.Text != null && textBox2.Text != "")
            {

                label2.Text = utilidades.RaizAproximada(Convert.ToDouble(textBox2.Text)).ToString();
            }
        }
    }
}
