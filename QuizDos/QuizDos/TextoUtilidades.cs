﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizDos.Utilidades
{
    class TextoUtilidades
    {
        internal bool EsPalindromo(string text)
        {
            string revert = "";

            for(int i = text.Length-1; i >= 0; i--)
            {
                revert += text[i];
            }

            if(revert == text)
            {
                return true;
            }

            return false;
        }

        internal string Subcadena(string text, decimal value1, decimal value2)
        {
            return text.Substring((int) value1 - 1, (int) value2);
        }

        internal bool EstaenMayuscula(string text)
        {
            foreach(char a in text)
            {
                if (!Char.IsUpper(a))
                {
                    return false;
                }
            }

            return true;
        }

        internal bool Valida(string text, string validation)
        {
            switch (validation)
            {
                case "A":
                    return EstaenMayuscula(text);
                case "a":
                    foreach (char a in text)
                    {
                        if (!Char.IsLower(a))
                        {
                            return false;
                        }
                    }
                    break;
                case "9":
                    foreach (char a in text)
                    {
                        if (!Char.IsNumber(a))
                        {
                            return false;
                        }
                    }
                    break;
                case "z":
                    foreach (char a in text)
                    {
                        if (!Char.IsLetter(a))
                        {
                            return false;
                        }
                    }
                    break;
                default:
                    return false;
            }

            return true;
        }

        internal bool Iguales(string text1, string text2)
        {
            if(text1 != text2)
            {
                return false;
            }

            return true;
        }
    }
}
