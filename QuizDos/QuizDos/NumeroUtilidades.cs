﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizDos.Utilidades
{
    class NumeroUtilidades
    {

        internal bool EsNarcisista(string v)
        {
            int sumatoria = 0;
            foreach(Char a in v){
                sumatoria += (int) Math.Pow(Convert.ToDouble(Char.GetNumericValue(a)), v.Length);
            }

            if(sumatoria.ToString() == v)
            {
                return true;
            }

            return false;
        }

        internal int RaizAproximada(double v)
        {
            for(int i = (int) v; i > 0; i--)
            {
                if(Esentero(Math.Sqrt(i)))
                {
                    return (int) Math.Sqrt(i);
                }
            }

            return -1;
        }

        private bool Esentero(double v)
        {
            if(v % 1 != 0) {
                return false;
            }

            return true;
        }
    }
}
