﻿using QuizDos.Utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizDos
{
    public partial class TextoInterfaz : Form
    {
        TextoUtilidades utilidades;
        String validation = "A";

        public TextoInterfaz()
        {
            InitializeComponent();
            utilidades = new TextoUtilidades();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (utilidades.EsPalindromo(textBox1.Text))
            {
                label1.Text = "Palindromo";
            }
            else
            {
                label1.Text = "No Palindromo";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label2.Text = utilidades.Subcadena(textBox2.Text, numericUpDown1.Value, numericUpDown2.Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (utilidades.EstaenMayuscula(textBox3.Text))
            {
                label3.Text = "MAYUSCULAS";
            }
            else
            {
                label3.Text = "NO MAYUSCULAS";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
                if (utilidades.Valida(textBox4.Text, validation))
            {
                label4.Text = "Valida";
            }
            else
            {
                label4.Text = "Invalida";
            }
        }

        private void Cambio(object sender, EventArgs e)
        {
            if (sender == radioButton1)
            {
                validation = "A";
            }
            else if (sender == radioButton2)
            {
                validation = "a";
            }
            else if (sender == radioButton3)
            {
                validation = "9";
            }
            else if (sender == radioButton4)
            {
                validation = "z";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            label5.Text = (utilidades.Iguales(textBox5.Text, textBox6.Text)) ? "iguales" : "Diferentes";
        }
    }
}
