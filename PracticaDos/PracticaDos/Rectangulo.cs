﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Rectangulo
    {
        public double Largo { get; set; }
        public double Ancho { get; set; }

        internal double CalcularArea()
        {
            return Largo * Ancho;
        }
    }
}
