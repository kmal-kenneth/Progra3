﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class CambioDivisas
    {
        public double Colones { get; set; }
        public double TipoCambio { get; set; }

        public CambioDivisas(double tipoCambio)
        {
            TipoCambio = tipoCambio;
        }

        internal double ComvertirADolares()
        {
            return Colones * TipoCambio;
        }
    }
}
