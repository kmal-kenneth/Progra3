﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Fecha
    {
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Año { get; set; }

        /// <summary>
        /// Inicia la fecha en 1/1/2000
        /// </summary>
        public Fecha()
        {
            Dia = 1;
            Mes = 1;
            Año = 2000;
        }

        /// <summary>
        /// Inicia la fecha en los valores establecidos.
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="año"></param>
        public Fecha(int dia, int mes, int año)
        {
            ModificarFecha(dia, mes, año);
        }

        /// <summary>
        /// Modifica la fecha segun los valores.
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="año"></param>
        internal void ModificarFecha(int dia, int mes, int año)
        {
            Dia = dia;
            Mes = mes;
            Año = año;
        }

        /// <summary>
        /// Retorna la fecha en el formato dia/mes/año.
        /// </summary>
        /// <returns></returns>
        internal string FechaToString()
        {
            return String.Format("{0}/{1}/{2}", Dia, Mes, Año);
        }

        /// <summary>
        /// Retorna la fecha en formato dia/mes/año pero el mes en palabras.
        /// </summary>
        /// <returns></returns>
        internal string FechaMesPalabra()
        {
            return String.Format("{0}/{1}/{2}", Dia, MesPalabras(Mes), Año);
        }

        /// <summary>
        /// Retorna el mes en texto.
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        string MesPalabras(int mes)
        {
            string s = "";

            switch (mes)
            {
                case 1:
                    s += "enero";
                    break;
                case 2:
                    s += "febrero";
                    break;
                case 3:
                    s += "marzo";
                    break;
                case 4:
                    s += "abril";
                    break;
                case 5:
                    s += "mayo";
                    break;
                case 6:
                    s += "junio";
                    break;
                case 7:
                    s += "julio";
                    break;
                case 8:
                    s += "agosto";
                    break;
                case 9:
                    s += "setiembre";
                    break;
                case 10:
                    s += "octubre";
                    break;
                case 11:
                    s += "noviembre";
                    break;
                case 12:
                    s += "diciembre";
                    break;
                default:
                    break;

            }

            return s;
        }
    }
}
