﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Circuferencia
    {
        public double Radio { get; set; }

        internal double calcularArea()
        {
            return Math.PI * Math.Pow(Radio, 2);
        }

        internal double CalcularPerimetro()
        {
            return 2 * Math.PI * Radio;
        }
    }
}
