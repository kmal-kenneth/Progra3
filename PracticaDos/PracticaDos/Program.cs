﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Program
    {
        static void Main(string[] args)
        {
            goto ET3;

        ET1:
            Circuferencia rueda = new Circuferencia();
            Circuferencia moneda = new Circuferencia { Radio = 1.4 };
            rueda.Radio = 10.2;
            Console.WriteLine("Área rueda: {0:0.00}", rueda.calcularArea());
            Console.WriteLine("Área moneda: {0:0.00}", moneda.calcularArea());
            Console.WriteLine("Perimetro rueda: {0:0.00}", rueda.CalcularPerimetro());
            Console.WriteLine("Perimetro moneda: {0:0.00}", moneda.CalcularPerimetro());

        ET2:
            Console.Write("Largo pared: ");
            var largo = Convert.ToDouble(Console.ReadLine());
            Console.Write("Ancho pared: ");
            var ancho = Convert.ToDouble(Console.ReadLine());

            Console.Write("Largo ventana: ");
            var largoVentana = Convert.ToDouble(Console.ReadLine());
            Console.Write("Ancho ventana: ");
            var anchoVentana = Convert.ToDouble(Console.ReadLine());

            var pared = new Rectangulo { Largo = largo, Ancho = ancho };
            var ventana = new Rectangulo { Largo = largoVentana, Ancho = anchoVentana };

            var area = pared.CalcularArea() - ventana.CalcularArea();

            Console.WriteLine("El tiempo necesario para pntar la pared es: {0} minutos", TiempoPintar(area));

            ET3:

            Console.ReadKey();
        }

        private static double TiempoPintar(double area)
        {
            return area * 10;
        }
    }
}
