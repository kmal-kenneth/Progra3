﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Gasolina
    {
        const float LITRO = 0.264172f;

        public double Galones { get; set; }
        public double PrecioLitro { get; set; }

        public Gasolina(double precioLitro)
        {
            PrecioLitro = precioLitro;
        }

        private double GalonesALitros()
        {
            return Galones / LITRO;
        }

        internal Double Precio()
        {
            return PrecioLitro * GalonesALitros();
        }
    }
}
