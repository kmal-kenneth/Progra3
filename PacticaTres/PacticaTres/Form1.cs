﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacticaTres
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void multiplicar_Click(object sender, EventArgs e)
        {
            double num1, num2;

            num1 = Convert.ToDouble(operando1.Text);
            num2 = Convert.ToDouble(operando2.Text);

            producto.Text =  (num1 * num2).ToString();

        }

        private void limpiar_Click(object sender, EventArgs e)
        {
            operando1.Clear();
            operando2.Clear();
            producto.Clear();
        }

        private void salir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
