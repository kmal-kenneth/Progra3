﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacticaTres
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double valor = Convert.ToDouble(textBox1.Text);
            double fanrenheit = 32.0 + 1.8 * valor;

            textBox2.Text = String.Format("{0:F3}", fanrenheit);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double valor = Convert.ToDouble(textBox1.Text);
            double centigrados = (valor - 32.0) / 1.8;

            textBox2.Text = String.Format("{0:F3}", centigrados);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
